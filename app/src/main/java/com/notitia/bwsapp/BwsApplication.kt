package com.notitia.bwsapp

import androidx.multidex.MultiDexApplication
import com.notitia.bwsapp.data.db.AppDatabase
import com.notitia.bwsapp.data.network.MyApi
import com.notitia.bwsapp.data.network.NetworkConnectionInterceptor
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.ui.auth.AuthViewModelFactory
import com.notitia.bwsapp.ui.finance.FinanceViewModelFactory
import com.notitia.bwsapp.ui.finance.tab_items.IncomeViewModelFactory
import com.notitia.bwsapp.ui.home.HomeViewModelFactory
import com.notitia.bwsapp.ui.notification.NotifyViewModelFactory
import com.notitia.bwsapp.ui.outreach.OutreachViewModelFactory
import com.notitia.bwsapp.ui.profile.ProfileViewModelFactory
import com.notitia.bwsapp.ui.profile.stepper.biodata.BioDataViewModelFactory
import com.notitia.bwsapp.ui.profile.stepper.history.HistoryViewModelFactory
import com.notitia.bwsapp.ui.profile.stepper.investigation.InvestigationViewModelFactory
import com.notitia.bwsapp.ui.setting.SettingViewModelFactory
import com.notitia.bwsapp.ui.setting.stepper.syncprofile.SyncProfileViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class BwsApplication : MultiDexApplication(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@BwsApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { DataRepository(instance(),instance()) }
        bind() from provider { AuthViewModelFactory(instance(),instance()) }
        bind() from provider { HomeViewModelFactory(instance(),instance()) }
        bind() from provider { ProfileViewModelFactory(instance(),instance()) }
        bind() from provider { BioDataViewModelFactory(instance(),instance()) }
        bind() from provider { SyncProfileViewModelFactory(instance(),instance()) }
        bind() from provider { InvestigationViewModelFactory(instance(),instance()) }
        bind() from provider { HistoryViewModelFactory(instance(),instance()) }
        bind() from provider { OutreachViewModelFactory(instance(),instance()) }
        bind() from provider { FinanceViewModelFactory(instance(),instance()) }
        bind() from provider { IncomeViewModelFactory(instance(),instance()) }
        bind() from provider { SettingViewModelFactory(instance(),instance()) }
        bind() from provider { NotifyViewModelFactory(instance(),instance()) }
    }

}