package com.notitia.bwsapp.data.db.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

data class NotProf (
    @Embedded
    var Profile:Profile?,

    @Embedded
    var Notify:Notification
)