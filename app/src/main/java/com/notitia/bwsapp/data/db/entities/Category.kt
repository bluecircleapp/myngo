package com.notitia.bwsapp.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "category")
data class Category (
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var Type: String? = null,
    var TypeId:String? = null,
    var Description:String? = null
)