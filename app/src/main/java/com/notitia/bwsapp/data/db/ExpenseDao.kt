package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.notitia.bwsapp.data.db.entities.Expense

@Dao
interface ExpenseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(expense: Expense) : Long

    @Query("SELECT * FROM expense ORDER BY id DESC")
    fun getExpenses() : LiveData<List<Expense>>
}