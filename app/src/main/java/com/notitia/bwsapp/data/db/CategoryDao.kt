package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.data.db.entities.Category
import com.notitia.bwsapp.data.db.entities.Income

@Dao
interface CategoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(category: Category) : Long

    @Query("SELECT * FROM category WHERE TypeId=:uid")
    fun getCategories(uid:String) : LiveData<List<Category>>

    @Query("SELECT COUNT(*) FROM category WHERE Type = :uid")
    suspend fun getCategory(uid:String?) : Int
}