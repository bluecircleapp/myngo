package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.db.entities.Medical
import com.notitia.bwsapp.data.db.entities.Tests

@Dao
interface TestDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(tests: Tests) : Long

    @Query("SELECT * FROM test")
    fun getTests() : LiveData<List<Tests>>

    @Query("SELECT * FROM test WHERE uniqueId = :uid")
    fun getSingleTest(uid:String?) : LiveData<Tests>

    @Query("SELECT COUNT(*) FROM test WHERE uniqueId = :uid")
    suspend fun getOneTest(uid:String?) : Int
}