package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.db.entities.Profile

@Dao
interface ProfileDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(profile: Profile) : Long

    @Query("SELECT * FROM profile WHERE active=:uid")
    fun getProfiles(uid:Boolean) : LiveData<List<Profile>>

    @Query("SELECT * FROM profile WHERE uniqueId = :uid")
    fun getProfile(uid:String?) : LiveData<Profile>

    @Query("SELECT * FROM profile WHERE outreach_uniqueId = :uid")
    fun getWithOutreach(uid:String?) : LiveData<List<Profile>>

    @Query("SELECT * FROM profile WHERE outreach_uniqueId IS NULL OR outreach_uniqueId=''")
    fun getNullOutreach() : LiveData<List<Profile>>

    @Query("SELECT COUNT(*) FROM profile WHERE uniqueId = :uid")
    suspend fun getSingleProfile(uid:String?) : Int

    @Query("SELECT COUNT(*) FROM profile WHERE active = :active")
    fun getPatientCount(active:Boolean) : LiveData<Int>

    @Query("SELECT COUNT(*) FROM profile WHERE outreach_uniqueId = :uid AND active=:active")
    fun getOutreachPatientCount(active:Boolean,uid:String?) : LiveData<Int>

    @Query("SELECT COUNT(*) FROM profile WHERE gender = :value AND active = :active")
    fun getMaleRecords(value:String,active:Boolean) : LiveData<Int>

    @Query("SELECT COUNT(*) FROM profile where gender = :value AND active = :active")
    fun getFemaleRecords(value:String,active:Boolean) : LiveData<Int>
}