package com.notitia.bwsapp.data.db.entities

import androidx.room.*
import com.notitia.bwsapp.util.DateConverter
import java.util.*

@Entity(tableName = "outreach",indices = [Index(value = ["uniqueId"],unique = true)])
data class Outreach (
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var uniqueId:String? = null,
    var name: String? = null,
    var location: String? = null,
    @ColumnInfo(name = "male_attendance")
    var maleAttendance:String? = null,
    @ColumnInfo(name = "female_attendance")
    var femaleAttendance:String? = null,
    var awareness: String? = null,
    var breastCancer:Boolean? = null,
    var cervicalCancer:Boolean? = null,
    var prostateCancer:Boolean? = null,
    var ultrasound:Boolean? = null,
    @TypeConverters(DateConverter::class)
    var startDate: Date? = null,
    @TypeConverters(DateConverter::class)
    var endDate: Date? = null
)