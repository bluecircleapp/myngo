package com.notitia.bwsapp.data.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.notitia.bwsapp.util.DateConverter
import java.util.*
@Entity(tableName = "expense")
data class Expense(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var uniqueId:String? = null,
    var amount: String? = null,
    var reasonForExpense: String? = null,
    @ColumnInfo(name = "date_of_expense")
    @TypeConverters(DateConverter::class)
    var date: Date? = null
)