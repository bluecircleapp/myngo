package com.notitia.bwsapp.data.db.entities

import androidx.room.*
import com.notitia.bwsapp.util.DateConverter
import java.util.*

@Entity(tableName = "medical", indices = [Index(value = ["profileId"])],foreignKeys = [ForeignKey(
    entity = Profile::class,
    parentColumns = arrayOf("uniqueId"),
    childColumns = arrayOf("profileId"),
    onDelete = ForeignKey.CASCADE
)]
)
data class Medical (
    @PrimaryKey(autoGenerate = true)
    var id:Long? = null,
    var profileId:String? = null,
    var uniqueId:String? = null,
    @ColumnInfo(name = "systolic_bp")
    var systolicBP: String? = null,
    @ColumnInfo(name = "diastolic_bp")
    var diastolicBP: String? = null,
    @ColumnInfo(name = "blood_glucose")
    var bloodGlucose: String? = null,
    var height: String? = null,
    var weight: String? = null,
    var BMI: String? = null
)