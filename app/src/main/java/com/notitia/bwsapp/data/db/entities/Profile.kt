package com.notitia.bwsapp.data.db.entities

import androidx.room.*
import com.notitia.bwsapp.util.DateConverter
import java.util.*

@Entity(tableName = "profile",indices = [Index(value = ["uniqueId"],unique = true)]
)
class Profile (
    @PrimaryKey(autoGenerate = true)
    var id:Long? = null,
    var uniqueId: String? = null,
    @ColumnInfo(name = "full_name")
    var surname: String? = null,
    var firstName: String? = null,
    var age: String? = null,
    var gender: String? = null,
    var parity: String? = null,
    @ColumnInfo(name = "phone_number")
    var phoneNumber: String? = null,
    var address: String? = null,
    var tribe: String? = null,
    var nationality: String? = null,
    @ColumnInfo(name = "outreach_uniqueId")
    var outreach_uniqueId: String? = null,
    @ColumnInfo(name = "date_created")
    @TypeConverters(DateConverter::class)
    var dateCreated: Date? = null,
    var active:Boolean? = null
)