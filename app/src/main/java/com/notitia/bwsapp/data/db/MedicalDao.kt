package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.db.entities.Medical

@Dao
interface MedicalDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(medical: Medical) : Long

    @Query("SELECT * FROM medical")
    fun getMedicals() : LiveData<List<Medical>>

    @Query("SELECT * FROM medical WHERE profileId = :uid")
    fun getSingleMedical(uid:String?) : LiveData<Medical>

    @Query("SELECT COUNT(*) FROM medical WHERE uniqueId = :uid")
    suspend fun getOneMedical(uid:String?) : Int
}