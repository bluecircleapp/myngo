package com.notitia.bwsapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.db.entities.Screening

@Dao
interface ScreeningDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(screening: Screening) : Long

    @Query("SELECT * FROM screening")
    fun getTest() : LiveData<List<Screening>>

    @Query("SELECT * FROM screening WHERE profileId = :uid")
    fun getSingleTest(uid:String?) : LiveData<Screening>

    @Query("SELECT COUNT(*) FROM screening WHERE uniqueId = :uid")
    suspend fun getOneScreening(uid:String?) : Int
}