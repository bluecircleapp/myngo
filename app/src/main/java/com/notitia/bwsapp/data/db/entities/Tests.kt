package com.notitia.bwsapp.data.db.entities

import androidx.room.*
import com.notitia.bwsapp.util.DateConverter
import java.util.*

@Entity(tableName = "test", indices = [Index(value = ["profileId"])],foreignKeys = [ForeignKey(
    entity = Profile::class,
    parentColumns = arrayOf("uniqueId"),
    childColumns = arrayOf("profileId"),
    onDelete = ForeignKey.CASCADE
)]
)
data class Tests (
    @PrimaryKey(autoGenerate = true)
    var id:Long? = null,
    var profileId:String? = null,
    var uniqueId:String? = null,
    @ColumnInfo(name = "papsmear_result")
    var papsmearResult: String? = null,
    @Embedded
    var papsReferred:PapReferred?,

    @ColumnInfo(name = "cryotherapy_result")
    var cryotherapyResult: String? = null,

    @ColumnInfo(name = "psa_result")
    var psaResult: String? = null,
    @Embedded
    var psaReferred:PsaReferred?,

    @ColumnInfo(name = "mammography_result")
    var mammographyResult: String? = null,
    @Embedded
    var mamReferred:MamReferred?,

    @ColumnInfo(name = "ultrasound_result")
    var ultrasoundResult: String? = null,
    @Embedded
    var ultraReferred:UltraReferred?
)

data class PapReferred(
    @ColumnInfo(name = "PapReferred")
    var referred: Boolean? = null,
    @ColumnInfo(name = "PapDescription")
    var description: String? = null
)
data class PsaReferred(
    @ColumnInfo(name = "PsaReferred")
    var referred: Boolean? = null,
    @ColumnInfo(name = "PsaDescription")
    var description: String? = null
)
data class MamReferred(
    @ColumnInfo(name = "MamReferred")
    var referred: Boolean? = null,
    @ColumnInfo(name = "MamDescription")
    var description: String? = null
)
data class UltraReferred(
    @ColumnInfo(name = "UltraReferred")
    var referred: Boolean? = null,
    @ColumnInfo(name = "UltraDescription")
    var description: String? = null
)