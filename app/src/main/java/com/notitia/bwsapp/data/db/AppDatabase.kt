package com.notitia.bwsapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.notitia.bwsapp.data.db.entities.*
import com.notitia.bwsapp.util.DateConverter


@Database(
    entities = [Income::class, Auth::class, Expense::class, Outreach::class, Profile::class,Medical::class,Screening::class,
    Tests::class,Category::class,Notification::class],
    version = 3, exportSchema = true
)
@TypeConverters(DateConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getIncomeDao(): IncomeDao
    abstract fun getAuthDao(): AuthDao
    abstract fun getProfileDao(): ProfileDao
    abstract fun getExpenseDao(): ExpenseDao
    abstract fun getOutreachDao(): OutreachDao
    abstract fun getMedicalDao(): MedicalDao
    abstract fun getScreeningDao():ScreeningDao
    abstract fun getTestDao():TestDao
    abstract fun getCategoryDao():CategoryDao
    abstract fun getNotifyDao():NotificationDao

    companion object {

        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        /*private val MIGRATION_2_3: Migration =
            object : Migration(2, 3) {
                override fun migrate(database: SupportSQLiteDatabase) {
                    database.execSQL(
                        "ALTER TABLE profile "
                                + "DROP COLUMN dateOfBirth"
                    )
                    database.execSQL(
                        "ALTER TABLE screening "
                                + "Add COLUMN doctor TEXT"
                    )
                }
            }*/

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "BwsApp.db").fallbackToDestructiveMigration().build()
    }
}