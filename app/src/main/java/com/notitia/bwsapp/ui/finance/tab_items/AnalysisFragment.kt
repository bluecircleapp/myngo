package com.notitia.bwsapp.ui.finance.tab_items

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.databinding.AnalysisFragmentBinding
import kotlinx.android.synthetic.main.analysis_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class AnalysisFragment : Fragment(),KodeinAware,TabListener {

    private lateinit var binding: AnalysisFragmentBinding

    override val kodein by kodein()

    private val factory: IncomeViewModelFactory by instance<IncomeViewModelFactory>()

    companion object {
        fun newInstance() = AnalysisFragment()
    }

    private lateinit var viewModel: IncomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.analysis_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this,factory).get(IncomeViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.tabListener = this

        viewModel.incomes.observe(viewLifecycleOwner, Observer {
            viewModel.expenses.observe(viewLifecycleOwner, Observer {int->
                if(!it.isNullOrEmpty() || !int.isNullOrEmpty()){
                    chartHold.visibility = View.GONE
                }
                any_chart_view.setChart(viewModel.iterateChartData(it,int))
            })
        })
    }

    override fun onStarted(repository: DataRepository) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String) {
        Toast.makeText(context,message,Toast.LENGTH_LONG).show()
    }

}
