package com.notitia.bwsapp.ui.profile.stepper.biodata

import androidx.lifecycle.ViewModel
import com.notitia.bwsapp.data.db.entities.Outreach
import com.notitia.bwsapp.data.db.entities.Profile
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.ui.profile.ProfileListener
import com.notitia.bwsapp.util.*
import java.sql.Date
import java.util.*
import kotlin.collections.HashMap

class BioDataViewModel(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
) : ViewModel() {
    val outreach = repository.getOutreach()

    var surname:String? = null
    var firstname:String? = null
    var phone_number:String? = null
    var gender:String? = null
    var parity:String? = null
    var address:String? = null
    var tribe:String? = null
    var age:String? = null
    var program:Int = 0

    val outreachHash:HashMap<Int,String?> = HashMap()

    var profileListener: ProfileListener? = null

    fun getOutreach(outreachList:List<Outreach>):ArrayList<String>{
        val programs = ArrayList<String>()
        programs.add("-- Select Outreach Program --")
        if(outreachList.isNotEmpty()) {
            for (i in outreachList.indices) {
                outreachHash[i+1] = outreachList[i].uniqueId
                programs.add(outreachList[i].name!!)
            }
        }
        return programs
    }

    fun createProfile():Boolean{
        val todayDOB = Date(Calendar.getInstance().timeInMillis)
        if(age!!.toInt()<10) {
        return false
        }else {
            val uniqueId = Utility().getUniqueID()
            prefs.savePreference(UniqueId, uniqueId)
            val outreach = outreachHash[program]
            val profile = Profile(
                null,
                uniqueId,
                surname,
                firstname,
                age.toString(),
                gender,
                parity,
                phone_number,
                address,
                tribe,
                "Nigeria",
                outreach,
                todayDOB,
                false
            )

            Coroutines.main {
                val profileId = repository.saveProfile(profile)
                prefs.savePreference(id, profileId.toString())
            }

            val person = HashMap<String, Any?>()
            person[UniqueId] = profile.uniqueId
            person[Surname] = profile.surname
            person[firstName] = profile.firstName
            person[Age] = profile.age
            person[Gender] = profile.gender
            person[Parity] = profile.parity
            person[PhoneNumber] = profile.phoneNumber
            person[Address] = profile.address
            person[Tribe] = profile.tribe
            person[Nationality] = profile.nationality
            person[OutreachId] = profile.outreach_uniqueId
            person[DateCreated] = todayDOB
            person[Active] = false

            Firebase().createMedicalDocument(Profile, UniqueId, uniqueId!!, person)
            return true
        }
    }

}
