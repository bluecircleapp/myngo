package com.notitia.bwsapp.ui.setting

import androidx.lifecycle.LiveData
import com.notitia.bwsapp.data.db.entities.Auth

interface SetListener {
    fun onStarted(id:Int)
    fun onSuccess(message:String)
    fun onError(message: String)
    fun onFirebaseFinish(response:LiveData<Any>,id:Int)
    fun onUserProcess(data:ArrayList<Auth>?)
}