package com.notitia.bwsapp.ui.profile.stepper.investigation

import android.view.View
import androidx.lifecycle.ViewModel
import com.notitia.bwsapp.data.db.entities.Screening
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.util.*
import java.util.*

class InvestigationViewModel(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
) : ViewModel() {
    var bloodSugar:String? = null
    var systolic:String? = null
    var diastolic:String? =null
    var height:String? = null
    var weight:String? = null
    var bmi:String? = null
    var test: String? = null
    var result:String? = null
    var venue:String?= null
    var doctor:String?= null
    var count:Int = 1

    val tests = ArrayList<String>()
    val results = ArrayList<String>()
    var investListener:InvestListener? = null

    fun addNewTest(view: View){
        investListener?.onStarted()
    }

    fun createTest(myCalendar: Calendar):Boolean{
        val date = Date(myCalendar.timeInMillis)

        val uniqueId = Utility().getUniqueID()
        tests.add(test!!)
        results.add(result!!)
        val test = Screening(null,prefs.getLastSavedAt(UniqueId)!!,uniqueId,tests.toString(),results.toString(),date,venue,doctor)
        Coroutines.main{
            repository.saveScreening(test)
        }

        val screening = HashMap<String, Any?>()
        screening[UniqueId] = test.uniqueId
        screening[ProfileId] = test.profileId
        screening[TestType] = test.testTypes
        screening[TestResult] = test.testResults
        screening[DateConducted] = test.dateConducted
        screening[VenueConducted] = test.venueConducted
        screening[ReportingDoctor] =test.doctor

        Firebase().createMedicalDocument(Screening, UniqueId,uniqueId!!,screening)
        createMedical()
        return true
    }

    fun createMedical():Boolean{
        if(!systolic.isNullOrEmpty() || !diastolic.isNullOrEmpty() || !bloodSugar.isNullOrEmpty() || !height.isNullOrEmpty()
            || !weight.isNullOrEmpty() || !bmi.isNullOrEmpty()) {
            val uniqueId = Utility().getUniqueID()

            val medical = com.notitia.bwsapp.data.db.entities.Medical(
                null,
                prefs.getLastSavedAt(UniqueId)!!,
                uniqueId,
                systolic,
                diastolic,
                bloodSugar,
                height,
                weight,
                bmi
            )

            Coroutines.main {
                repository.saveMedical(medical)
            }

            val medicals = HashMap<String, Any?>()
            medicals[UniqueId] = medical.uniqueId
            medicals[ProfileId] = medical.profileId
            medicals[Systolic] = medical.systolicBP
            medicals[Diastolic] = medical.diastolicBP
            medicals[BloodSugar] = medical.bloodGlucose
            medicals[Height] = medical.height
            medicals[Weight] = medical.weight
            medicals[BMI] = medical.BMI

            Firebase().createMedicalDocument(Medical, UniqueId, uniqueId!!, medicals)
        }
        return true
    }
}
