package com.notitia.bwsapp.ui.profile.stepper.history

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.RadioGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.databinding.HistoryFragmentBinding
import com.notitia.bwsapp.ui.profile.ProfileListener
import com.notitia.bwsapp.util.SetDate
import com.notitia.bwsapp.util.show
import com.stepstone.stepper.BlockingStep
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.history_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.math.pow

class HistoryFragment : Fragment(), BlockingStep,KodeinAware,ProfileListener,AdapterView.OnItemSelectedListener,
    RadioGroup.OnCheckedChangeListener{

    companion object {
        fun newInstance() = HistoryFragment()
    }

    private lateinit var viewModel: HistoryViewModel
    override val kodein by kodein()
    private val factory: HistoryViewModelFactory by instance<HistoryViewModelFactory>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val binding: HistoryFragmentBinding = DataBindingUtil.inflate(inflater,
            R.layout.history_fragment,container,false)
        viewModel = ViewModelProvider(this,factory).get(HistoryViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        viewModel.profileListener = this

        binding.papsmaearEdt.onItemSelectedListener = this
        binding.papgroup.setOnCheckedChangeListener(this)
        binding.psagroup.setOnCheckedChangeListener(this)
        binding.mamgroup.setOnCheckedChangeListener(this)
        binding.ultragroup.setOnCheckedChangeListener(this)



        return binding.root
    }

    override fun onBackClicked(callback: StepperLayout.OnBackClickedCallback?) {
        callback?.goToPrevStep()
    }

    override fun onSelected() {
        return
    }

    override fun onCompleteClicked(callback: StepperLayout.OnCompleteClickedCallback?) {
        callback?.stepperLayout?.showProgress("Please Wait")
        if(viewModel.createTest()) {
            Toast.makeText(context,"Profile Created Successfully",Toast.LENGTH_LONG).show()
            callback?.complete()
            callback?.stepperLayout?.hideProgress()
            activity?.finish()
        }else{
            callback?.stepperLayout?.hideProgress()
            Toast.makeText(context,"Cannot Add Medical Records", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onNextClicked(callback: StepperLayout.OnNextClickedCallback?) {

    }

    override fun verifyStep(): VerificationError? {
        return null
    }


    override fun onError(error: VerificationError) {
        return
    }

    override fun onStarted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        TODO("Not yet implemented")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent!!.id){
            R.id.papsmaearEdt->if(!parent.selectedItem.toString().equals("Select Pap Result",ignoreCase = true))
            { viewModel.papSmear = parent.selectedItem.toString()}
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        if(checkedId==R.id.papyes){
            wherepap.hint = "Where"
            wherepap.show()
            viewModel.paprefer = true
        }
        if(checkedId==R.id.papno){
            wherepap.hint = "Reason"
            wherepap.show()
            viewModel.paprefer = false
        }
        if(checkedId==R.id.psayes){
            wherepsa.hint = "Where"
            wherepsa.show()
            viewModel.psarefer = true
        }
        if(checkedId==R.id.psano){
            wherepsa.hint = "Reason"
            wherepsa.show()
            viewModel.psarefer = false
        }
        if(checkedId==R.id.mamyes){
            wheremam.hint = "Where"
            wheremam.show()
            viewModel.mamrefer = true
        }
        if(checkedId==R.id.mamno){
            wheremam.hint = "Reason"
            wheremam.show()
            viewModel.mamrefer = false
        }
        if(checkedId==R.id.ultrayes){
            whereultra.hint = "Where"
            whereultra.show()
            viewModel.ultrarefer = true
        }
        if(checkedId==R.id.ultrano){
            whereultra.hint = "Reason"
            whereultra.show()
            viewModel.ultrarefer = false
        }

    }

}
