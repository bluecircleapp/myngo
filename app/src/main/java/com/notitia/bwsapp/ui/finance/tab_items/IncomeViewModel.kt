package com.notitia.bwsapp.ui.finance.tab_items

import android.view.View
import androidx.lifecycle.ViewModel
import com.anychart.AnyChart
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.charts.Pie
import com.anychart.enums.Align
import com.anychart.enums.LegendLayout
import com.notitia.bwsapp.data.db.entities.Category
import com.notitia.bwsapp.data.db.entities.Expense
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.util.Utility
import java.lang.Exception
import kotlin.math.exp

class IncomeViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
) : ViewModel() {
    val incomes = repository.getIncome()
    val expenses = repository.getExpense()
    val incomeCate = repository.getCategories("1")
    val expenseCate = repository.getCategories("2")

    val incomeAmount:ArrayList<Int> = ArrayList()
    val expenseAmount:ArrayList<Int> = ArrayList()

    var tabListener:TabListener? = null

    fun onIncomeClick(view:View){
        tabListener?.onStarted(repository)
    }

    fun onExpenseClick(view:View){
        tabListener?.onStarted(repository)
    }

    fun iterateChartData(income:List<Income>, expense:List<Expense>): Pie?{
        try{
            incomeAmount.add(0)
            expenseAmount.add(0)
            if(!income.isNullOrEmpty() &&!expense.isNullOrEmpty()){
                for(e in income.indices){
                    incomeAmount[0] = incomeAmount[0] + income[e].amount!!.toInt()
                }
                for(e in expense.indices){
                    expenseAmount[0] = expenseAmount[0] + expense[e].amount!!.toInt()
                }
            }
            return anychart(incomeAmount[0], expenseAmount[0])
        }catch (e:Exception){
            tabListener?.onError("An error occurred during analysis")
            return null
        }
    }

    fun anychart(income: Int, expense:Int): Pie {
        val pie: Pie = AnyChart.pie()
        val data = ArrayList<DataEntry>()
        data.add(ValueDataEntry("Total Income", income))
        data.add(ValueDataEntry("Total Expenditure", expense))
        pie.title("Total Revenue Flow")
        pie.data(data)

        pie.labels().position("outside")

        pie.legend().title().enabled(true)
        pie.legend().title()
            .text("Financial Data")
            .padding(0.0, 0.0, 10.0, 0.0)

        pie.legend()
            .position("center-bottom")
            .itemsLayout(LegendLayout.HORIZONTAL)
            .align(Align.CENTER)
        return pie
    }

    fun getIncome(incomes:List<Category>):ArrayList<String>{
        val years = ArrayList<String>()
        years.add("-- Income Category --")
        for (i in incomes.indices) {
            years.add(incomes[i].Type!!)
        }
        return years
    }
}
