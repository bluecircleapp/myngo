package com.notitia.bwsapp.ui.finance

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.databinding.ActivityFinanceBinding
import com.notitia.bwsapp.ui.finance.tab_items.AnalysisFragment
import com.notitia.bwsapp.ui.finance.tab_items.ExpenseFragment
import com.notitia.bwsapp.ui.finance.tab_items.IncomeFragment
import com.notitia.bwsapp.util.GenericPagerAdapter
import kotlinx.android.synthetic.main.activity_finance.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class FinanceActivity : AppCompatActivity(),KodeinAware,FinanceListener {

    override val kodein by kodein()

    private val factory: FinanceViewModelFactory by instance()
    private lateinit var viewmodel: FinanceViewModel

    private lateinit var binding: ActivityFinanceBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Finance"

        binding = DataBindingUtil.setContentView(this,R.layout.activity_finance)
        viewmodel = ViewModelProvider(this,factory).get(FinanceViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this

        viewmodel.financeListener = this

        setUpViewPager()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private fun setUpViewPager() { //SetUp pager Adapter
        val pagerAdapter = GenericPagerAdapter(supportFragmentManager)
        pagerAdapter.addFragment(
            IncomeFragment.newInstance(),"Income"
        )
        pagerAdapter.addFragment(ExpenseFragment(), "Expenditure")
        pagerAdapter.addFragment(AnalysisFragment(), "Analysis")
        binding.viewPager.adapter = pagerAdapter
        binding.tabLayout.setupWithViewPager(viewPager, true)
    }

}
