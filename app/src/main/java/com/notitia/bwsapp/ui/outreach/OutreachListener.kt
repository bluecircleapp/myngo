package com.notitia.bwsapp.ui.outreach

import androidx.lifecycle.LiveData

interface OutreachListener {
    fun onStarted()
    fun onSuccess(message:String)
    fun onError(message:String)
    fun onFirebaseFinish(response: LiveData<Any>)
}