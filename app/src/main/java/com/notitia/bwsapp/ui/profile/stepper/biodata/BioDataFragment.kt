package com.notitia.bwsapp.ui.profile.stepper.biodata

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.databinding.BioDataFragmentBinding
import com.notitia.bwsapp.ui.profile.ProfileListener
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.show
import com.stepstone.stepper.BlockingStep
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.bio_data_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class BioDataFragment : Fragment(),BlockingStep,KodeinAware,ProfileListener,AdapterView.OnItemSelectedListener,
    TextWatcher{

    companion object {
        fun newInstance() = BioDataFragment()
    }

    private lateinit var viewModel: BioDataViewModel
    private lateinit var binding: BioDataFragmentBinding

    override val kodein by kodein()

    private val factory: BioDataViewModelFactory by instance<BioDataViewModelFactory> ()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.bio_data_fragment, container, false)
        viewModel = ViewModelProvider(this, factory).get(BioDataViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        viewModel.profileListener = this

        binding.gender.onItemSelectedListener = this
        binding.outreachSpinner.onItemSelectedListener = this

        binding.phone.addTextChangedListener(this)

        viewModel.outreach.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            val programs = viewModel.getOutreach(it)
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(context!!,
                R.layout.spinner_layout, programs)
            outreachSpinner.adapter=adapter
        })


        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        return binding.root
    }

    override fun onBackClicked(callback: StepperLayout.OnBackClickedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSelected() {
        return
    }

    override fun onCompleteClicked(callback: StepperLayout.OnCompleteClickedCallback?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onNextClicked(callback: StepperLayout.OnNextClickedCallback?) {
        callback?.stepperLayout?.showProgress("Please Wait")
        if(viewModel.createProfile()) {
            callback?.goToNextStep()
            callback?.stepperLayout?.hideProgress()
        }else{
            if(viewModel.age!!.toInt()<10){
                callback?.stepperLayout?.hideProgress()
                binding.profileAge.error = "Patient age cannot be less than 10. Check date of birth."
                Toast.makeText(context,"Patient age cannot be less than 10. Check date of birth.",Toast.LENGTH_LONG).show()
            }else {
                callback?.stepperLayout?.hideProgress()
                Toast.makeText(context, "Cannot Create Patient Profile", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun verifyStep(): VerificationError? {
        return when {
            profile_name.text.toString().isEmpty() -> {
                profile_name.error = "Field Empty"
                VerificationError("Patient surname must be provided")
            }
            firstname.text.toString().isEmpty() -> {
                firstname.error = "Field Empty"
                VerificationError("Patient firstname must be provided")
            }
            profile_age.text.toString().isEmpty() -> {
                profile_age.error = "Field Empty"
                VerificationError("Patient age must be provided")
            }
            phone.text.toString().isEmpty() -> {
                phone.error = "Field Empty"
                VerificationError("Patient phone number must be provided")
            }
            phone.text.toString().length<11 -> {
                phone.error = "Phone Number Invalid"
                VerificationError("Phone number should be 11 digits")
            }
            gender.selectedItem.toString().equals("gender",true) -> {
                VerificationError("Patient gender must be provided")
            }
            else -> {
                null
            }
        }
    }

    override fun onError(error: VerificationError) {
        Toast.makeText(context,error.errorMessage,Toast.LENGTH_LONG).show()
    }

    override fun onStarted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {

    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        TODO("Not yet implemented")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent?.id){
            R.id.gender->if(parent.selectedItem.toString().equals("female",ignoreCase = true))
            {parity.show()
                viewModel.gender = parent.selectedItem.toString()}else{viewModel.gender = parent.selectedItem.toString()
                parity.hide()}
            R.id.outreachSpinner->if(!parent.selectedItem.toString().equals("-- Select Outreach Program --",ignoreCase = true))
            { viewModel.program = parent.selectedItemPosition}
        }
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if(s!!.length<11){
            binding.phone.error = "Phone Number is Invalid"
        }
    }
}
