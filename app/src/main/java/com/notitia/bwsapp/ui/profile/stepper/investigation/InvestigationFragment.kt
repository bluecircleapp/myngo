package com.notitia.bwsapp.ui.profile.stepper.investigation

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.databinding.InvestigationFragmentBinding
import com.notitia.bwsapp.util.SetDate
import com.stepstone.stepper.BlockingStep
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import kotlinx.android.synthetic.main.investigation_fragment.*
import kotlinx.android.synthetic.main.test_params.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.math.pow

class InvestigationFragment : Fragment(),BlockingStep,KodeinAware,AdapterView.OnItemSelectedListener,TextWatcher,
    InvestListener,View.OnClickListener {

    companion object {
        fun newInstance() = InvestigationFragment()
    }

    private lateinit var viewModel: InvestigationViewModel
    override val kodein by kodein()
    private val factory: InvestigationViewModelFactory by instance<InvestigationViewModelFactory>()
    private lateinit var myCalendar: Calendar
    var addTest:View? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: InvestigationFragmentBinding = DataBindingUtil.inflate(inflater,
            R.layout.investigation_fragment,container,false)
        viewModel = ViewModelProvider(this,factory).get(InvestigationViewModel::class.java)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this
        viewModel.investListener = this

        binding.heightEdt.addTextChangedListener(this)
        binding.weightEdt.addTextChangedListener(this)

        binding.testResultSpinner.onItemSelectedListener = this
        binding.testTypeSpinner.onItemSelectedListener = this

        myCalendar = SetDate(binding.testDateEdt,null,context!!).getCalendar()
        //activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return binding.root
    }
    override fun onBackClicked(callback: StepperLayout.OnBackClickedCallback?) {
        callback?.goToPrevStep()
    }

    override fun onSelected() {
        return
    }

    override fun onCompleteClicked(callback: StepperLayout.OnCompleteClickedCallback?) {

    }

    override fun onNextClicked(callback: StepperLayout.OnNextClickedCallback?) {
        callback?.stepperLayout?.showProgress("Please Wait")
        testResult()
        if(viewModel.createTest(myCalendar)) {
            callback?.goToNextStep()
            callback?.stepperLayout?.hideProgress()
            //activity?.finish()
        }else{
            callback?.stepperLayout?.hideProgress()
            viewModel.tests.clear()
            viewModel.results.clear()
            Toast.makeText(context,"Cannot Add Patients Test", Toast.LENGTH_LONG).show()
        }
    }

    override fun verifyStep(): VerificationError? {
        return when {
            testTypeSpinner.selectedItem.toString().equals("select test type",true) -> {
                VerificationError("Patient test type must be provided")
            }
            testResultSpinner.selectedItem.toString().equals("select test result",true) -> {
                VerificationError("Patient test result must be provided")
            }
            else -> {
                null
            }
        }
    }

    override fun onError(error: VerificationError) {
        Toast.makeText(context,error.errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent!!.id){
            R.id.testTypeSpinner->if(!parent.selectedItem.toString().equals("select test type",ignoreCase = true))
                {viewModel.test = parent.selectedItem.toString()}
            R.id.testResultSpinner->if(!parent.selectedItem.toString().equals("select test result",ignoreCase = true))
                {viewModel.result = parent.selectedItem.toString()}
        }
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if(s.hashCode() == heightEdt.text.hashCode()){
            if (s!!.isNotEmpty()) {
                if (weightEdt.text.toString().isNotEmpty()) {
                    val bmi: Double =
                        weightEdt.text.toString().toDouble() / heightEdt.text.toString().toDouble().pow(2.0)
                    bmiEdt.setText(bmi.toString())
                }
            }
        }
        if(s.hashCode() == weightEdt.text.hashCode()){
            if (s!!.isNotEmpty()) {
                if (heightEdt.text.toString().isNotEmpty()) {
                    val bmi: Double =
                        weightEdt.text.toString().toDouble() / heightEdt.text.toString().toDouble().pow(2.0)
                    bmiEdt.setText(bmi.toString())
                }
            }
        }
    }

    override fun onStarted() {
        if(viewModel.count<=3) {
            addTest?.removeTest?.visibility = View.GONE
            addTest = LayoutInflater.from(context).inflate(R.layout.test_params, null) as View
            addTest?.tag = viewModel.count
            addTest?.removeTest?.tag = viewModel.count
            addTest?.spinnerTest?.id = ((viewModel.count).toString()+"1").toInt()
            addTest?.spinnerType?.id = ((viewModel.count).toString()+"2").toInt()
            addTest?.removeTest?.setOnClickListener(this)
            testParams.addView(addTest)
            viewModel.count++
        }else{
            Toast.makeText(context,"Cannot add more tests",Toast.LENGTH_LONG).show()
        }
    }

    override fun onSuccess(message: String) {
        TODO("Not yet implemented")
    }

    override fun onError(message: String) {
        TODO("Not yet implemented")
    }

    override fun onClick(v: View?) {
        val index = v?.removeTest?.tag
        testParams.removeViewAt(index.toString().toInt())
        viewModel.count--
        val view1:View? = testParams.getChildAt(viewModel.count-1)
        view1?.removeTest?.visibility = View.VISIBLE
    }

    private fun testResult(){
        for(i in 1 until viewModel.count) {
            if(i==viewModel.count){
                break
            }else {
                val view: View? = testParams.getChildAt(i)
                val spinnerTest: Spinner? = view?.findViewById((i.toString() + "1").toInt())
                val spinnerType: Spinner? = view?.findViewById((i.toString() + "2").toInt())
                viewModel.tests.add(spinnerTest?.selectedItem.toString())
                viewModel.results.add(spinnerType?.selectedItem.toString())
            }
        }
    }
}
