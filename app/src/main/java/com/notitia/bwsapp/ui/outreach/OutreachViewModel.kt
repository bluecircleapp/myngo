package com.notitia.bwsapp.ui.outreach

import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.Gson
import com.notitia.bwsapp.data.db.entities.Outreach
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.util.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class OutreachViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
): ViewModel() {
    val outreach = repository.getOutreach()
    val outreachProfile = repository.getProfileOutreachCount()
    val program = repository.getSingleProgram()

    var title:String? = null
    var location:String? = null
    var maleAttendance:String? = null
    var femaleAttendance:String? = null
    var awareness:String? = null
    var startDate:String? = null
    var endDate:String? = null
    var cervical:Boolean? = null
    var breast:Boolean? = null
    var prostate:Boolean? = null
    var utrasound:Boolean? = null
    var uniqueIdText:String? = null

    var outreachListener:OutreachListener? = null

    fun createOutreach(myCalendar: Calendar, myCalendar1: Calendar):Boolean {
        val start = Date(myCalendar.timeInMillis)
        val end = Date(myCalendar1.timeInMillis)
        return if(vallidateField()) {
            val uniqueId = Utility().getUniqueID()
            val outreach = Outreach(null, uniqueIdText, title, location, maleAttendance, femaleAttendance, awareness, breast,
                cervical, prostate, utrasound, start, end)
            Coroutines.main {
                repository.saveOutreach(outreach)
            }

            val progs = HashMap<String, Any?>()
            progs[UniqueId] = outreach.uniqueId
            progs[Title] = outreach.name
            progs[Location] = outreach.location
            progs[StartDate] = outreach.startDate
            progs[EndDate] = outreach.endDate
            progs[Awareness] = outreach.awareness
            progs[MaleAttendance] = outreach.maleAttendance
            progs[FemaleAttendance] = outreach.femaleAttendance
            progs[BreastCancer] = outreach.breastCancer
            progs[CervicalCancer] = outreach.cervicalCancer
            progs[ProstateCancer] = outreach.prostateCancer
            progs[Ultrasound] = outreach.ultrasound

            Firebase().createMedicalDocument(Outreach, UniqueId,uniqueId!!,progs)
            true
        }else{
            false
        }
    }

    fun updateOutreach():Boolean {
        return if(vallidateField()) {
            val outreach = Outreach(program.value!!.id, program.value!!.uniqueId, title, location, maleAttendance, femaleAttendance, awareness, breast,
                cervical, prostate, utrasound, SimpleDateFormat(myFormat, Locale.UK).parse(startDate!!),
                SimpleDateFormat(myFormat, Locale.UK).parse(endDate!!))
            Coroutines.main {
                repository.updateOutrech(outreach.uniqueId!!,outreach.startDate!!,outreach.endDate!!,outreach.maleAttendance!!
                    ,outreach.femaleAttendance!!)
            }

            val progs = HashMap<String, Any?>()
            progs[UniqueId] = outreach.uniqueId
            progs[Title] = outreach.name
            progs[Location] = outreach.location
            progs[StartDate] = outreach.startDate
            progs[EndDate] = outreach.endDate
            progs[Awareness] = outreach.awareness
            progs[MaleAttendance] = outreach.maleAttendance
            progs[FemaleAttendance] = outreach.femaleAttendance
            progs[BreastCancer] = outreach.breastCancer
            progs[CervicalCancer] = outreach.cervicalCancer
            progs[ProstateCancer] = outreach.prostateCancer
            progs[Ultrasound] = outreach.ultrasound

            Firebase().updateDocument(Outreach,outreach.uniqueId!!,progs)
            true
        }else{
            false
        }
    }

    fun vallidateField():Boolean{
        return !(title.isNullOrEmpty() || location.isNullOrEmpty() || maleAttendance.isNullOrEmpty() || femaleAttendance.isNullOrEmpty()
                || awareness.isNullOrEmpty() || startDate.isNullOrEmpty() || endDate.isNullOrEmpty())
    }
    fun outreachSync(){
        outreachListener?.onStarted()
        val response = Firebase().getDocument(Outreach)
        outreachListener?.onFirebaseFinish(response)
    }
    fun processOutreach(data: QuerySnapshot){
        if(!data.isEmpty) {
            data.forEach {
                val document = Gson().toJson(it.data)
                val obj = JSONObject(document)
                val profile =
                    Outreach(null,
                        obj.getString(UniqueId),
                        obj.getString(Title),
                        obj.getString(Location),
                        obj.getString(MaleAttendance),
                        obj.getString(FemaleAttendance),
                        obj.getString(Awareness),
                        obj.getBoolean(BreastCancer),
                        obj.getBoolean(CervicalCancer),
                        obj.getBoolean(ProstateCancer),
                        obj.getBoolean(Ultrasound),
                        Converter().timeStamptoDate(obj.getString(StartDate)),
                        Converter().timeStamptoDate(obj.getString(EndDate)))
                Coroutines.main{
                    repository.checkSaveOutreach(profile)
                }
            }
            outreachListener?.onSuccess("")
        }else{
            outreachListener?.onError("")
        }
    }
    fun deleteOutreachLocal(outreach:Outreach){
        Coroutines.main {
            repository.deleteOutreach(outreach)
        }
        /*Firebase().deleteOutreach(Outreach, UniqueId, outreach.uniqueId!!)
        outreachListener?.onSuccess("Outreach Program Deleted Successfully")*/
    }
}