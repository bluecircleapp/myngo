package com.notitia.bwsapp.ui.profile.stepper.history

import androidx.lifecycle.ViewModel
import com.notitia.bwsapp.data.db.entities.*
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.ui.profile.ProfileListener
import com.notitia.bwsapp.util.*

class HistoryViewModel(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
) : ViewModel() {
    var papSmear:String? = null
    var via:String? = null
    var cryoBool:Boolean? = false
    var cryo:String? = "Done"
    var psa:String? = null
    var mammo:String? = null
    var ultra:String? = null


    var paprefer:Boolean? = null
    var papdescription:String? = null
    var psarefer:Boolean? = null
    var psadescription:String? = null
    var mamrefer:Boolean? = null
    var mamdescription:String? = null
    var ultrarefer:Boolean? = null
    var ultradescription:String? = null


    var profileListener: ProfileListener? = null


    fun createTest():Boolean{
        if(!papSmear.isNullOrEmpty() || !cryo.isNullOrEmpty() || !psa.isNullOrEmpty() || !mammo.isNullOrEmpty()
            || !ultra.isNullOrEmpty()) {

            val uniqueId = Utility().getUniqueID()

            if(!cryoBool!!){
                cryo = "Not Done"
            }

            val medical = Tests(null, prefs.getLastSavedAt(UniqueId)!!, uniqueId, papSmear, PapReferred(paprefer,papdescription),
                cryo, psa, PsaReferred(psarefer,psadescription), mammo,
                MamReferred(mamrefer,mamdescription), ultra,UltraReferred(ultrarefer,ultradescription)
            )

            Coroutines.main {
                repository.saveTest(medical)
            }
            val medicals = HashMap<String, Any?>()
            medicals[UniqueId] = medical.uniqueId
            medicals[ProfileId] = medical.profileId
            medicals[PapSmear] = medical.papsmearResult
            //medicals[PapSmearDate] = medical.papsmearDate
            medicals[PapReferred] = medical.papsReferred!!.referred
            medicals[PapDescription] = medical.papsReferred!!.description

            medicals[CryotherapyResult] = medical.cryotherapyResult
            //medicals[CryotherapyDate] = medical.cryotherapyDate
            medicals[PsaResult] = medical.psaResult
            //medicals[PsaDate] = medical.psaDate
            medicals[PsaReferred] = medical.psaReferred!!.referred
            medicals[PsaDescription] = medical.psaReferred!!.description

            medicals[MammographyResult] = medical.mammographyResult
            //medicals[MammographyDate] = medical.mammographyDate
            medicals[MamReferred] = medical.mamReferred!!.referred
            medicals[MamDescription] = medical.mamReferred!!.description

            medicals[UltrasoundResult] = medical.ultrasoundResult
            //medicals[UltrasoundDate] = medical.ultrasoundDate
            medicals[UltraReferred] = medical.ultraReferred!!.referred
            medicals[UltraDescription] = medical.ultraReferred!!.description

            Firebase().createMedicalDocument(Test, UniqueId, uniqueId!!, medicals)
        }
        return true
    }

}
