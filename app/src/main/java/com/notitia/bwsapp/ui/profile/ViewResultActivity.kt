package com.notitia.bwsapp.ui.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Tests
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.databinding.ActivityNewTestBinding
import com.notitia.bwsapp.databinding.ActivityViewResultBinding
import com.notitia.bwsapp.util.Case
import com.notitia.bwsapp.util.CaseCatgory
import iammert.com.expandablelib.ExpandCollapseListener
import iammert.com.expandablelib.ExpandableLayout
import iammert.com.expandablelib.Section
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ViewResultActivity : AppCompatActivity(),KodeinAware,ProfileListener, ExpandableLayout.Renderer<CaseCatgory, Case>,
    ExpandCollapseListener.CollapseListener<CaseCatgory>, ExpandCollapseListener.ExpandListener<CaseCatgory> {

    override val kodein by kodein()

    private val factory: ProfileViewModelFactory by instance<ProfileViewModelFactory>()
    private val prefs: PreferenceProvider by instance<PreferenceProvider>()
    private lateinit var viewmodel: ProfileViewModel

    val myFormat = "E dd-MMM-yyyy"
    lateinit var sdf: SimpleDateFormat
    var count = 0
    val sections:ArrayList<Long?> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Patient Test Results"

        val binding: ActivityViewResultBinding = DataBindingUtil.setContentView(this,R.layout.activity_view_result)
        viewmodel = ViewModelProvider(this,factory).get(ProfileViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.profileListener = this

        binding.el.setRenderer(this)

        viewmodel.tests.observe(this, androidx.lifecycle.Observer {
            if(it.isNotEmpty()){
                for(e in it.indices){
                    if(!sections.contains(it[e].id)) {
                        count++
                        binding.el.addSection(getSection(it[e], false)!!)
                        sections.add(it[e].id)
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        TODO("Not yet implemented")
    }

    override fun renderChild(view: View?, model: Case?, parentPosition: Int, childPosition: Int) {
        (view!!.findViewById<View>(R.id.testid) as TextView).text = model!!.id.toString()
        (view.findViewById<View>(R.id.paps) as TextView).text = model.ca15
        (view.findViewById<View>(R.id.cryo) as TextView).text = model.ca125
        (view.findViewById<View>(R.id.psa) as TextView).text = model.ca25
        (view.findViewById<View>(R.id.mammo) as TextView).text = model.antigen
        (view.findViewById<View>(R.id.ultra) as TextView).text = model.psa
    }

    override fun renderParent(view: View?, model: CaseCatgory?, isExpanded: Boolean, parentPosition: Int) {
        (view!!.findViewById<View>(R.id.tvParent) as TextView).text = model!!.name.toUpperCase(
            Locale.UK)
        view.findViewById<View>(R.id.arrow)
            .setBackgroundResource(if (isExpanded) R.drawable.arrow_up else R.drawable.arrow_down)
    }

    override fun onCollapsed(parentIndex: Int, parent: CaseCatgory?, view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onExpanded(parentIndex: Int, parent: CaseCatgory?, view: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun getSection(item: Tests, check: Boolean): Section<CaseCatgory, Case>? {
        val section = Section<CaseCatgory, Case>()
        val fruitCategory = CaseCatgory("Test Results")
        val fruit1 = Case(item.id,
            if (item.papsmearResult.isNullOrEmpty()) "NIL" else item.papsmearResult,
            if(item.cryotherapyResult.isNullOrEmpty()) "NIL" else item.cryotherapyResult,
            if(item.psaResult.isNullOrEmpty()) "NIL" else item.psaResult+"ng",
            if(item.mammographyResult.isNullOrEmpty()) "NIL" else item.mammographyResult,
            if(item.ultrasoundResult.isNullOrEmpty()) "NIL" else item.ultrasoundResult)
        if (check) {
            section.children.clear()
        }
        if (!section.children.contains(fruit1)) {
            section.parent = fruitCategory
            section.children.add(fruit1)
            section.expanded = false
        }
        return section
    }
}
