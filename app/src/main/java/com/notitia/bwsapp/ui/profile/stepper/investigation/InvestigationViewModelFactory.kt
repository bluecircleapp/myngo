package com.notitia.bwsapp.ui.profile.stepper.investigation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository

@Suppress("UNCHECKED_CAST")
class InvestigationViewModelFactory(
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return InvestigationViewModel(repository,prefs) as T
    }
}