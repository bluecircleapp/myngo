package com.notitia.bwsapp.ui.finance

import androidx.lifecycle.ViewModel
import com.notitia.bwsapp.data.db.entities.Outreach
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.util.Coroutines
import java.util.*

class FinanceViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
): ViewModel() {
    var financeListener:FinanceListener? = null

}