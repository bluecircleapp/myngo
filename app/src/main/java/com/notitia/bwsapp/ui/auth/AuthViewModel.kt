package com.notitia.bwsapp.ui.auth

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository
import com.notitia.bwsapp.util.*
import org.json.JSONObject
import java.util.*

class AuthViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
): ViewModel() {
    var username:String? = null
    var password:String? = null
    var passcode:String? = null
    var authListener:AuthListener? = null

    private val _knowSignin = MutableLiveData<String?>().apply {
        value = prefs.getLastSavedAt(user_name)
    }

    val knowSignin: LiveData<String?> = _knowSignin

    fun onForgotClick(@Suppress("UNUSED_PARAMETER")view:View){
        authListener?.onForgetDisplay()
    }

    fun resetPassword(email:String?){
        if(email.isNullOrEmpty()){
            return
        }
        val response = Firebase().sendPasswordRest(email)
        authListener?.onFirebaseFinish(response, 3)
    }
    fun onLoginClick(@Suppress("UNUSED_PARAMETER")view: View){
        authListener?.onStarted()
        if(!username.isNullOrEmpty() && !password.isNullOrEmpty()) {
            if(prefs.checkPreference(user_name)){
                if(password.equals(prefs.getLastSavedAt(pass_word), true)){
                    if(prefs.getLastBoolean(activated)!!){
                        prefs.savePreference(Version,"1.10")
                        authListener?.onStopped()
                        authListener?.onSuccess("Login Successful")
                    }
                    else{
                        authListener?.onStopped()
                        authListener?.onError("This account is deactivated")
                    }
                }else{
                    authListener?.onStopped()
                    authListener?.onError("The email and password combination does not exist")
                }
            }else {
                val response = Firebase().siginIn(username!!, password!!)
                authListener?.onFirebaseFinish(response, 1)
            }

        }
        else{
            authListener?.onError("Email or Password Incorrect")
            authListener?.onStopped()
            return
        }
    }

    fun userDetails(){
        val response = Firebase().getSingleDocument(Users, user_name, username!!)
        authListener?.onFirebaseFinish(response,2)
    }

    fun loginUser(user:String?){
        if(!user.isNullOrEmpty()) {
            val json = JSONObject(user)
            Coroutines.main {
                val auth =
                    Auth(1,
                        json.getString(name),
                        json.getString(employee),
                        json.getString(user_name),
                        Date(Calendar.getInstance().timeInMillis),
                        json.getString(roleId),
                        json.getBoolean(activated))
                repository.saveAuth(auth)
                prefs.savePreference(roleId,auth.role)
                prefs.savePreference(pass_word,password)
                prefs.savePreference(user_name,auth.username)
                prefs.savePreference(Outreach,"0")
                prefs.savePreference(Version,"1.10")
                prefs.saveBooleanPreference(activated,auth.activated)
                if(auth.activated!!) {
                    authListener?.onStopped()
                    authListener?.onSuccess("Login Successful")
                }else{
                    authListener?.onStopped()
                    authListener?.onError("This account is deactivated")
                }
            }
        }else{
            authListener?.onStopped()
            authListener?.onError("The email and password combination does not exist")
        }
    }
}