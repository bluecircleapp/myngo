package com.notitia.bwsapp.ui.setting.setting_items

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.QuerySnapshot
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.UserAdapter
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.databinding.ActivityViewUsersBinding
import com.notitia.bwsapp.ui.setting.SetListener
import com.notitia.bwsapp.ui.setting.SettingViewModel
import com.notitia.bwsapp.ui.setting.SettingViewModelFactory
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.show
import kotlinx.android.synthetic.main.activity_profile.recycler_view
import kotlinx.android.synthetic.main.activity_view_users.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ViewUsersActivity : AppCompatActivity(),KodeinAware,SetListener {
    override val kodein by kodein()

    private val factory: SettingViewModelFactory by instance<SettingViewModelFactory>()
    private lateinit var viewmodel: SettingViewModel

    private var listPatient:ArrayList<Auth>? = ArrayList()
    private var profileAdapter: UserAdapter? = UserAdapter(listPatient)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "View Users"

        val binding: ActivityViewUsersBinding = DataBindingUtil.setContentView(this,R.layout.activity_view_users)
        viewmodel = ViewModelProvider(this,factory).get(SettingViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel
        viewmodel.setListener = this

        viewmodel.getUsers()

        recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = profileAdapter
            /*addOnItemTouchListener(
                RecyclerTouchListener(
                    context,
                    recycler_view,
                    object : ClickListener {
                        override fun onClick(view: View, position: Int) {
                            if(listPatient!!.isNotEmpty()) {
                                val patient = listPatient!![position]

                            }
                        }

                        override fun onLongClick(view: View, position: Int) {}
                    })
            )*/
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted(id: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onError(message: String) {
        progress_bar.hide()
        emptyState1.text = "No User Data Available"
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        response.observe(this, Observer {
            viewmodel.processUserList(it as QuerySnapshot)
        })
    }

    override fun onUserProcess(data: ArrayList<Auth>?) {
        if(!data.isNullOrEmpty()){
            listPatient?.clear()
            for(e in data.indices){
                listPatient?.add(data[e])
            }
            profileAdapter?.notifyDataSetChanged()
            progress_bar.hide()
            emptyState1.hide()
            recycler_view.show()
        }else{
            progress_bar.hide()
            emptyState1.text = "No User Data Available"
        }
    }
}
