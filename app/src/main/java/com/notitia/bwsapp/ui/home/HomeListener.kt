package com.notitia.bwsapp.ui.home

interface HomeListener {
    fun onSuccess(id:Int?)
    fun onStarted()
    fun onError(message:String?)
}