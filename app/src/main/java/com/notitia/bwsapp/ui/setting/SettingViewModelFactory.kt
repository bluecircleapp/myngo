package com.notitia.bwsapp.ui.setting

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository

class SettingViewModelFactory (
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SettingViewModel(repository,prefs) as T
        }
}