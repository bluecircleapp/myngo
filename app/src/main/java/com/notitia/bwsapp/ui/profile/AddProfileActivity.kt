package com.notitia.bwsapp.ui.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.StepperAdapter
import com.stepstone.stepper.StepperLayout

class AddProfileActivity : AppCompatActivity() {

    private var mStepperLayout: StepperLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Add Profile"
        setContentView(R.layout.activity_add_profile)

        mStepperLayout = findViewById(R.id.stepperLayout)
        mStepperLayout?.adapter = StepperAdapter(this.supportFragmentManager, this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
