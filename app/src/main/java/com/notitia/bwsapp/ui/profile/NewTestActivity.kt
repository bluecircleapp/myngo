package com.notitia.bwsapp.ui.profile

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.R
import com.notitia.bwsapp.databinding.ActivityNewTestBinding
import com.notitia.bwsapp.util.show
import com.notitia.bwsapp.util.toast
import kotlinx.android.synthetic.main.activity_new_test.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class NewTestActivity : AppCompatActivity(),KodeinAware,ProfileListener,AdapterView.OnItemSelectedListener,
    RadioGroup.OnCheckedChangeListener{

    override val kodein by kodein()

    private val factory: ProfileViewModelFactory by instance<ProfileViewModelFactory>()
    private lateinit var viewmodel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Add New Test"
        val binding: ActivityNewTestBinding = DataBindingUtil.setContentView(this,R.layout.activity_new_test)
        viewmodel = ViewModelProvider(this,factory).get(ProfileViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel.profileListener = this

        binding.papsmaearEdt.onItemSelectedListener = this
        binding.papgroup.setOnCheckedChangeListener(this)
        binding.psagroup.setOnCheckedChangeListener(this)
        binding.mamgroup.setOnCheckedChangeListener(this)
        binding.ultragroup.setOnCheckedChangeListener(this)


    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSuccess(message: String) {
        toast(message)
        finish()
    }

    override fun onError(message: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        TODO("Not yet implemented")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent!!.id){
            R.id.papsmaearEdt->if(!parent.selectedItem.toString().equals("Select Pap Result",ignoreCase = true))
            { viewmodel.papSmear = parent.selectedItem.toString() }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        if(checkedId==R.id.papyes){
            wherepap.hint = "Where"
            wherepap.show()
            viewmodel.paprefer = true
        }
        if(checkedId==R.id.papno){
            wherepap.hint = "Reason"
            wherepap.show()
            viewmodel.paprefer = false
        }
        if(checkedId==R.id.psayes){
            wherepsa.hint = "Where"
            wherepsa.show()
            viewmodel.psarefer = true
        }
        if(checkedId==R.id.psano){
            wherepsa.hint = "Reason"
            wherepsa.show()
            viewmodel.psarefer = false
        }
        if(checkedId==R.id.mamyes){
            wheremam.hint = "Where"
            wheremam.show()
            viewmodel.mamrefer = true
        }
        if(checkedId==R.id.mamno){
            wheremam.hint = "Reason"
            wheremam.show()
            viewmodel.mamrefer = false
        }
        if(checkedId==R.id.ultrayes){
            whereultra.hint = "Where"
            whereultra.show()
            viewmodel.ultrarefer = true
        }
        if(checkedId==R.id.ultrano){
            whereultra.hint = "Reason"
            whereultra.show()
            viewmodel.ultrarefer = false
        }

    }
}

