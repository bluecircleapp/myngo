package com.notitia.bwsapp.ui.setting

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.firestore.QuerySnapshot
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Auth
import com.notitia.bwsapp.databinding.ActivitySettingBinding
import com.notitia.bwsapp.ui.auth.LoginActivity
import com.notitia.bwsapp.ui.setting.setting_items.*
import com.notitia.bwsapp.util.hide
import com.notitia.bwsapp.util.toast
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.initialize_layout.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SettingActivity : AppCompatActivity(),KodeinAware,SetListener {

    override val kodein by kodein()

    private val factory: SettingViewModelFactory by instance<SettingViewModelFactory>()
    private lateinit var viewmodel: SettingViewModel
    private var alertDialog: AlertDialog.Builder? = null
    private var alert: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Settings"
        val binding: ActivitySettingBinding = DataBindingUtil.setContentView(this,R.layout.activity_setting)
        viewmodel = ViewModelProvider(this,factory).get(SettingViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewmodel = viewmodel
        viewmodel.setListener = this

        viewmodel.knowRole.observe(this, Observer {
            if(!it.equals("admin",true)){
                create.hide()
                users.hide()
            }
        })
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted(id: Int) {
        if(id==1){
            Intent(this, UserActivity::class.java).also {
                startActivity(it)
            }
        }
        if(id==2){
            Intent(this, ViewUsersActivity::class.java).also {
                startActivity(it)
            }
        }
        if(id==3){
            Intent(this, UpdateActivity::class.java).also {
                startActivity(it)
            }
        }
        if(id==4){
            val view = LayoutInflater.from(this).inflate(R.layout.initialize_layout, null) as View
            view.name.hide()
            view.synctext?.text = "Please Wait while we download and sync online records"
            alertDialog = AlertDialog.Builder(this)
            alertDialog?.setView(view)
            alertDialog?.setCancelable(false)
            alert = alertDialog?.create()
            alert?.show()
            /*Intent(this, SyncActivity::class.java).also {
                startActivity(it)
            }*/
        }
        if(id==5){
            Intent(this,DownloadActivity::class.java).also{
                startActivity(it)
            }
        }
    }

    override fun onSuccess(message: String) {
        Handler().postDelayed({
            alert?.dismiss()
            toast(message)
        },3000L)
    }

    override fun onError(message: String) {
        Handler().postDelayed({
            alert?.dismiss()
            toast(message)
        },3000L)
    }

    override fun onFirebaseFinish(response: LiveData<Any>,id:Int) {
        when (id) {
            1 -> {
                response.observe(this, Observer {
                    if (it as Boolean) {
                        if (it) {
                            Intent(this, LoginActivity::class.java).also { it1 ->
                                startActivity(it1)
                            }
                            finish()
                        }
                        viewmodel.clearPref()
                    }
                })
            }
            2 -> {
                response.observe(this, Observer {
                    viewmodel.processProfiles((it as QuerySnapshot))
                })
            }
            3 -> {
                response.observe(this, Observer {
                    viewmodel.processMedical((it as QuerySnapshot))
                })
            }
            4 -> {
                response.observe(this, Observer {
                    viewmodel.processScreening((it as QuerySnapshot))
                })
            }
            else -> {
                response.observe(this, Observer {
                    viewmodel.processTest((it as QuerySnapshot))
                })
            }
        }
    }

    override fun onUserProcess(data: ArrayList<Auth>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
