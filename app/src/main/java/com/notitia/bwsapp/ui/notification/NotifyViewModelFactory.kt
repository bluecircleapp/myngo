package com.notitia.bwsapp.ui.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository

class NotifyViewModelFactory (
    private val repository: DataRepository,
    private val prefs: PreferenceProvider
    ): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return NotificationViewModel(repository,prefs) as T
        }
}