package com.notitia.bwsapp.ui.finance

interface FinanceListener {
    fun onStarted()
    fun onSuccess(message:String)
    fun onError(message:String)
}