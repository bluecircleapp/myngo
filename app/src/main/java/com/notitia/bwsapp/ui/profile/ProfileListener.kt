package com.notitia.bwsapp.ui.profile

import androidx.lifecycle.LiveData

interface ProfileListener {
    fun onStarted()
    fun onSuccess(message:String)
    fun onError(message:String)
    fun onFirebaseFinish(response: LiveData<Any>,id:Int)
}