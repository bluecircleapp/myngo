package com.notitia.bwsapp.ui.notification

import androidx.lifecycle.ViewModel
import com.notitia.bwsapp.data.preferences.PreferenceProvider
import com.notitia.bwsapp.data.repositories.DataRepository

class NotificationViewModel(
    private val repository: DataRepository,
    private val prefs:PreferenceProvider
): ViewModel() {
    val notifications = repository.getNotifies()
}