package com.notitia.bwsapp.ui.auth

import androidx.lifecycle.LiveData

interface AuthListener {
    fun onStarted()
    fun onError(message:String)
    fun onSuccess(message:String)
    fun onStopped()
    fun onForgetDisplay()
    fun onFirebaseFinish(response: LiveData<Any>, i: Int)
}