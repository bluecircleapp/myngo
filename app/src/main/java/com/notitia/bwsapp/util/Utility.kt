package com.notitia.bwsapp.util

import android.app.AlertDialog
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.android.material.textfield.TextInputLayout
import com.notitia.bwsapp.R
import com.notitia.bwsapp.adapter.CustomAdapter
import com.notitia.bwsapp.data.db.entities.Expense
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.firebase.Firebase
import com.notitia.bwsapp.data.repositories.DataRepository
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Utility {

    private val sdf = SimpleDateFormat(myFormat, Locale.UK)

    fun getUniqueID():String?{
        return "${randomString(5)}${randomInt(5)}"
    }

    fun randomString(size: Int): String {
        val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        return (source).map { it }.shuffled().subList(0, size).joinToString("")
    }
    fun randomInt(size: Int): String {
        val source = "1234567890"
        return (source).map { it }.shuffled().subList(0, size).joinToString("")
    }

    fun incomeDialog(ctx: Context,repository: DataRepository,spinner:ArrayList<String>){
        val view = LayoutInflater.from(ctx).inflate(R.layout.add_income, null) as View
        val saveIncome = view.findViewById(R.id.saveIncomeBtn) as Button
        val amount = view.findViewById(R.id.amountEdt) as EditText
        val other = view.findViewById(R.id.donorNameEdt) as EditText
        val otherLay = view.findViewById(R.id.layEdit) as TextInputLayout
        val donor = view.findViewById(R.id.incomeSpinner) as Spinner
        val date = view.findViewById(R.id.dateDonatedEdt) as EditText
        val calendar = SetDate(date,null,ctx).getCalendar()
        var donorString:String? = null
        donor.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(parent?.id){
                    R.id.incomeSpinner-> donorString =
                        if(parent.selectedItem.toString().equals("other",ignoreCase = true)) {otherLay.show()
                            other.text.toString()
                        }else{ otherLay.hide()
                            parent.selectedItem.toString()
                        }
                }
            }

        }
        other.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                donorString = s.toString()
            }

        })
        val mAdapter = CustomAdapter(ctx, spinner)
        donor.adapter = mAdapter
        val dialog = AlertDialog.Builder(ctx)
            .setView(view)
            .setCancelable(true)
            .show()
        saveIncome.setOnClickListener{
            if(validateFields(amount.text.toString(),donorString,date.text.toString())){
                val date = Date(calendar.timeInMillis)
                val uniqueId = Utility().getUniqueID()
                Coroutines.main {
                    repository.saveIncome(
                        Income(null, uniqueId, amount.text.toString(), donorString, date)
                    )
                }
                val income = HashMap<String, Any?>()
                income[UniqueId] = uniqueId
                income[Amount] = amount.text.toString()
                income[Donor] = donorString
                income[Date] = date
                Firebase().createDocument(Income,income)
                dialog.dismiss()
            }else{
                Toast.makeText(ctx,"Cannot Submit Empty Fields",Toast.LENGTH_LONG).show()
                dialog.dismiss()
            }
        }
    }

    fun expenseDialog(ctx: Context,repository: DataRepository,spinner:ArrayList<String>){
        val view = LayoutInflater.from(ctx).inflate(R.layout.add_expenditure, null) as View
        val dialog = AlertDialog.Builder(ctx)
            .setView(view)
            .setCancelable(true)
            .show()
        val saveIncome = view.findViewById(R.id.saveExpenseBtn) as Button
        val amount = view.findViewById(R.id.expenseAmountEdt) as EditText
        val other = view.findViewById(R.id.donorNameEdt) as EditText
        val otherLay = view.findViewById(R.id.layEdit) as TextInputLayout
        val donor = view.findViewById(R.id.incomeSpinner) as Spinner
        val date = view.findViewById(R.id.dateExpenseEdt) as EditText
        val calendar = SetDate(date,null,ctx).getCalendar()
        var donorString:String? = null
        donor.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(parent?.id){
                    R.id.incomeSpinner-> donorString =
                        if(parent.selectedItem.toString().equals("other",ignoreCase = true)) {otherLay.show()
                            other.text.toString()
                        }else{ otherLay.hide()
                            parent.selectedItem.toString()
                        }
                }
            }

        }
        other.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                donorString = s.toString()
            }

        })
        val mAdapter = CustomAdapter(ctx, spinner)
        donor.adapter = mAdapter
        saveIncome.setOnClickListener{
            if(validateFields(amount.text.toString(),donorString,date.text.toString())){
                val date = Date(calendar.timeInMillis)
                val uniqueId = Utility().getUniqueID()
                Coroutines.main {
                    repository.saveExpense(
                        Expense(null, uniqueId, amount.text.toString(), donorString, date)
                    )
                }
                val expense = HashMap<String, Any?>()
                expense[UniqueId] = uniqueId
                expense[Amount] = amount.text.toString()
                expense[Desc] = donorString
                expense[Date] = date
                Firebase().createDocument(Expenses,expense)
                dialog.dismiss()
            }else{
                Toast.makeText(ctx,"Cannot Submit Empty Fields",Toast.LENGTH_LONG).show()
                dialog.dismiss()
            }
        }
    }

    fun editIncomeDialog(ctx: Context,repository: DataRepository,income:Income,spinner:ArrayList<String>){
        val view = LayoutInflater.from(ctx).inflate(R.layout.add_income, null) as View
        val saveIncome = view.findViewById(R.id.saveIncomeBtn) as Button
        val title = view.findViewById(R.id.addNewIncome) as TextView
        val amount = view.findViewById(R.id.amountEdt) as EditText
        val otherLay = view.findViewById(R.id.layEdit) as TextInputLayout
        val other = view.findViewById(R.id.donorNameEdt) as EditText
        val donor = view.findViewById(R.id.incomeSpinner) as Spinner
        val date = view.findViewById(R.id.dateDonatedEdt) as EditText
        SetDate(date,null,ctx).getCalendar()

        var donorString:String? = null
        val mAdapter = CustomAdapter(ctx, spinner)
        donor.adapter = mAdapter
        saveIncome.text = "Update"
        title.text = "Edit Income"
        amount.setText(income.amount)
        donor.setSelection(mAdapter.getPosition(income.donor!!))
        if(!spinner.contains(income.donor!!)){
            donor.hide()
            otherLay.show()
            other.setText(income.donor)
            donorString = income.donor
        }

        donor.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(parent?.id){
                    R.id.incomeSpinner-> donorString =
                        if(parent.selectedItem.toString().equals("other",ignoreCase = true)) {otherLay.show()
                            other.text.toString()
                        }else{ otherLay.hide()
                            parent.selectedItem.toString()
                        }
                }
            }

        }
        other.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                donorString = s.toString()
            }

        })

        date.setText(sdf.format(income.date!!))
        val dialog = AlertDialog.Builder(ctx)
            .setView(view)
            .setCancelable(true)
            .show()
        saveIncome.setOnClickListener{
            if(validateFields(amount.text.toString(),donorString,date.text.toString())){
                income.amount = amount.text.toString()
                income.donor = donorString
                income.date = SimpleDateFormat(myFormat, Locale.UK).parse(date.text.toString())
                Coroutines.main {
                    repository.saveIncome(income)
                }
                val income1 = HashMap<String, Any?>()
                income1[Amount] = income.amount
                income1[Donor] = income.donor
                income1[Date] = income.date
                Firebase().updateDocument(Income,income.uniqueId!!,income1)
                dialog.dismiss()
            }else{
                Toast.makeText(ctx,"Cannot Submit Empty Fields",Toast.LENGTH_LONG).show()
                dialog.dismiss()
            }
        }
    }
    fun editExpenseDialog(ctx: Context,repository: DataRepository,expense: Expense,spinner:ArrayList<String>){
        val view = LayoutInflater.from(ctx).inflate(R.layout.add_expenditure, null) as View
        val saveIncome = view.findViewById(R.id.saveExpenseBtn) as Button
        val title = view.findViewById(R.id.headingExpense) as TextView
        val amount = view.findViewById(R.id.expenseAmountEdt) as EditText
        val otherLay = view.findViewById(R.id.layEdit) as TextInputLayout
        val other = view.findViewById(R.id.donorNameEdt) as EditText
        val donor = view.findViewById(R.id.incomeSpinner) as Spinner
        val date = view.findViewById(R.id.dateExpenseEdt) as EditText
        SetDate(date,null,ctx).getCalendar()
        var donorString:String? = null
        val mAdapter = CustomAdapter(ctx, spinner)
        donor.adapter = mAdapter

        saveIncome.text = "Update"
        title.text = "Edit Expenses"
        amount.setText(expense.amount)
        donor.setSelection(mAdapter.getPosition(expense.reasonForExpense!!))
        if(!spinner.contains(expense.reasonForExpense!!)){
            donor.hide()
            otherLay.show()
            other.setText(expense.reasonForExpense)
            donorString = expense.reasonForExpense
        }
        donor.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(parent?.id){
                    R.id.incomeSpinner-> donorString =
                        if(parent.selectedItem.toString().equals("other",ignoreCase = true)) {otherLay.show()
                            other.text.toString()
                        }else{ otherLay.hide()
                            parent.selectedItem.toString()
                        }
                }
            }

        }
        other.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                donorString = s.toString()
            }

        })

        date.setText(sdf.format(expense.date!!))
        val dialog = AlertDialog.Builder(ctx)
            .setView(view)
            .setCancelable(true)
            .show()

        saveIncome.setOnClickListener{
            if(validateFields(amount.text.toString(),donorString,date.text.toString())){
                expense.amount = amount.text.toString()
                expense.reasonForExpense = donorString
                expense.date = SimpleDateFormat(myFormat, Locale.UK).parse(date.text.toString())
                Coroutines.main {
                    repository.saveExpense(expense)
                }
                val expense1 = HashMap<String, Any?>()
                expense1[Amount] = expense.amount
                expense1[Desc] = expense.reasonForExpense
                expense1[Date] = expense.date
                Firebase().updateDocument(Expenses,expense.uniqueId!!,expense1)
                dialog.dismiss()
            }else{
                Toast.makeText(ctx,"Cannot Submit Empty Fields",Toast.LENGTH_LONG).show()
                dialog.dismiss()
            }
        }
    }

    fun validateFields(amount:String?, donor:String?, date:String?):Boolean =
        !amount.isNullOrEmpty() || !donor.isNullOrEmpty() || !date.isNullOrEmpty()


}