package com.notitia.bwsapp.util

import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.EditText
import android.widget.TimePicker
import java.util.*


internal class SetTime(private val editText: EditText, private val ctx: Context) : OnFocusChangeListener,
    OnTimeSetListener {
    private val myCalendar: Calendar

    init {
        this.editText.onFocusChangeListener = this
        this.myCalendar = Calendar.getInstance()

    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        // TODO Auto-generated method stub
        if (hasFocus) {
            val hour = myCalendar.get(Calendar.HOUR_OF_DAY)
            val minute = myCalendar.get(Calendar.MINUTE)
            TimePickerDialog(this.ctx, this, hour, minute, true).show()
        }
    }

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        // TODO Auto-generated method stub
        this.editText.setText("$hourOfDay:$minute")
    }

}