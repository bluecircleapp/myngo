package com.notitia.bwsapp.util

import android.content.Context
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.wang.avi.AVLoadingIndicatorView

fun Context.toast(message:String){
    Toast.makeText(this,message,Toast.LENGTH_LONG).show()
}
fun LinearLayout.show(){
    visibility = View.VISIBLE
}
fun LinearLayout.hide(){
    visibility = View.GONE
}
fun ProgressBar.show(){
    visibility = View.VISIBLE
}
fun ProgressBar.hide(){
    visibility = View.GONE
}
fun Button.show(){
    visibility = View.VISIBLE
}
fun Button.hide(){
    visibility = View.GONE
}
fun EditText.show(){
    visibility = View.VISIBLE
}
fun EditText.hide(){
    visibility = View.GONE
}
fun TextView.show(){
    visibility = View.VISIBLE
}
fun TextView.hide(){
    visibility = View.GONE
}
fun Spinner.show(){
    visibility = View.VISIBLE
}
fun Spinner.hide(){
    visibility = View.GONE
}
fun RecyclerView.show(){
    visibility = View.VISIBLE
}
fun RecyclerView.hide(){
    visibility = View.GONE
}
fun AVLoadingIndicatorView.view(){
    visibility = View.VISIBLE
}
fun AVLoadingIndicatorView.remove(){
    visibility = View.GONE
}
fun View.snackbar(message: String){
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).also { snackbar ->
        snackbar.setAction("Ok") {
            snackbar.dismiss()
        }
    }.show()
}