package com.notitia.bwsapp.util

import java.io.IOException
import java.io.PrintWriter
import java.io.Writer

class CSVWriter {
    private var pw: PrintWriter? = null

    private var separator = 0.toChar()

    private var quotechar = 0.toChar()

    private var escapechar = 0.toChar()

    private var lineEnd: String? = null

    /** The character used for escaping quotes.  */
    val DEFAULT_ESCAPE_CHARACTER = '"'

    /** The default separator to use if none is supplied to the constructor.  */
    val DEFAULT_SEPARATOR = ','

    /**
     * The default quote character to use if none is supplied to the
     * constructor.
     */
    val DEFAULT_QUOTE_CHARACTER = '"'

    /** The quote constant to use when you wish to suppress all quoting.  */
    val NO_QUOTE_CHARACTER = '\u0000'

    /** The escape constant to use when you wish to suppress all escaping.  */
    val NO_ESCAPE_CHARACTER = '\u0000'

    /** Default line terminator uses platform encoding.  */
    val DEFAULT_LINE_END = "\n"

    fun CSVWriter(writer: Writer?, separator: Char, quotechar: Char, escapechar: Char, lineEnd: String?) {
        pw = PrintWriter(writer!!)
        this.separator = separator
        this.quotechar = quotechar
        this.escapechar = escapechar
        this.lineEnd = lineEnd
    }


    fun CSVWriter(writer: Writer?) {
        pw = PrintWriter(writer!!)
        this.separator = DEFAULT_SEPARATOR
        this.quotechar = DEFAULT_QUOTE_CHARACTER
        this.escapechar = DEFAULT_ESCAPE_CHARACTER
        this.lineEnd = DEFAULT_LINE_END
    }


    fun writeNext(nextLine: Array<String?>?) {
        if (nextLine == null) return
        val sb = StringBuffer()
        for (i in nextLine.indices) {
            if (i != 0) {
                sb.append(separator)
            }
            val nextElement = nextLine[i] ?: continue
            if (quotechar != NO_QUOTE_CHARACTER) sb.append(quotechar)
            for (element in nextElement) {
                val nextChar = element
                if (escapechar != NO_ESCAPE_CHARACTER && nextChar == quotechar) {
                    sb.append(escapechar).append(nextChar)
                } else if (escapechar != NO_ESCAPE_CHARACTER && nextChar == escapechar) {
                    sb.append(escapechar).append(nextChar)
                } else {
                    sb.append(nextChar)
                }
            }
            if (quotechar != NO_QUOTE_CHARACTER) sb.append(quotechar)
        }
        sb.append(lineEnd)
        pw?.write(sb.toString())
    }

    /**
     * Flush underlying stream to writer.
     *
     * @throws IOException if bad things happen
     */
    @Throws(IOException::class)
    fun flush() {
        pw?.flush()
    }

    /**
     * Close the underlying stream writer flushing any buffered content.
     *
     * @throws IOException if bad things happen
     */
    @Throws(IOException::class)
    fun close() {
        pw?.flush()
        pw?.close()
    }
}