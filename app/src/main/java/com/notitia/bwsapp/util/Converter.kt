package com.notitia.bwsapp.util

import com.google.firebase.Timestamp
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class Converter {
    fun toDate(date:String): Date{
        val dateFormat = SimpleDateFormat("yyyy-MM-dd",Locale.UK)
        return dateFormat.parse(date)!!
    }

    fun timeStamptoDate(date:String):Date{
        val obj = JSONObject(date)
        var timestamp = Timestamp(obj.getLong(seconds),obj.getInt(nanoseconds))
        return timestamp.toDate()
    }
}