package com.notitia.bwsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.NotProf
import com.notitia.bwsapp.data.db.entities.Notification
import com.notitia.bwsapp.data.db.entities.Profile
import java.text.DateFormat

class NotifyAdapter(patients:ArrayList<NotProf>?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<NotProf>? = patients

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.profile_name)
        var about: TextView = view.findViewById(R.id.profile_about)
        var address: TextView = view.findViewById(R.id.profile_address)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.notify_layout, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder

            val item: NotProf = mRecyclerViewItems!![position]
            menuItemHolder.name.text = "${item.Profile!!.surname} ${item.Profile!!.firstName}"
            menuItemHolder.about.text = "${item.Notify.test} Test"
            val endDate =
                DateFormat.getDateInstance().format(item.Notify.duedate!!)
            menuItemHolder.address.text = "$endDate"
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}