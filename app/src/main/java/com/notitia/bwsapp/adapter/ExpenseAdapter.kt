package com.notitia.bwsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Expense
import com.notitia.bwsapp.data.db.entities.Income
import com.notitia.bwsapp.data.db.entities.Outreach
import java.text.DateFormat

class ExpenseAdapter(patients:ArrayList<Expense>?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<Expense>? = patients

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var donor: TextView = view.findViewById(R.id.itemExpenseReason)
        var amount: TextView = view.findViewById(R.id.itemExpenseAmount)
        var date: TextView = view.findViewById(R.id.itemExpenseDate)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.expense_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder

            val item: Expense = mRecyclerViewItems!![position]
            val startDate =
                DateFormat.getDateInstance(DateFormat.MEDIUM)
                    .format(item.date!!)
            menuItemHolder.donor.text = "${item.reasonForExpense}"
            menuItemHolder.amount.text = "${item.amount}"
            menuItemHolder.date.text = "$startDate"
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}