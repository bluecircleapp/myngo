package com.notitia.bwsapp.adapter

import android.content.Context
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentManager
import com.notitia.bwsapp.ui.profile.stepper.biodata.BioDataFragment
import com.notitia.bwsapp.ui.profile.stepper.history.HistoryFragment
import com.notitia.bwsapp.ui.profile.stepper.investigation.InvestigationFragment
import com.notitia.bwsapp.ui.setting.stepper.syncfinance.SyncFinanceFragment
import com.notitia.bwsapp.ui.setting.stepper.syncprofile.SyncProfileFragment
import com.stepstone.stepper.Step
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter
import com.stepstone.stepper.viewmodel.StepViewModel

class Stepper1Adapter(fm: FragmentManager, context: Context) :
    AbstractFragmentStepAdapter(fm, context) {

    override fun getCount(): Int {
        return 2
    }

    override fun createStep(position: Int): Step {
        return when (position) {
            0 -> SyncProfileFragment()
            else-> SyncFinanceFragment()
        }
    }
    @NonNull
    override fun getViewModel(position: Int): StepViewModel {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        val builder = StepViewModel.Builder(context)
        when (position) {
            0 -> builder.setTitle("Profile Synchronization")
            1 -> builder.setTitle("Outreach Synchronization")
            else -> throw IllegalArgumentException("Unsupported position: $position")
        }
        return builder.create()
    }
}