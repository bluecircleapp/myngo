package com.notitia.bwsapp.adapter

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import java.util.*

class CustomAdapter(private val ctx: Context, private val asr: ArrayList<String>) : BaseAdapter(), SpinnerAdapter {
    override fun getCount(): Int {
        return asr.size
    }

    override fun getItem(i: Int): Any {
        return asr[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val txt = TextView(ctx)
        txt.setPadding(16, 16, 16, 16)
        txt.textSize = 18f
        txt.gravity = Gravity.CENTER_VERTICAL
        txt.text = asr[position]
        txt.setTextColor(Color.parseColor("#000000"))
        return txt
    }

    override fun getView(i: Int, view: View?, viewgroup: ViewGroup?): View {
        val txt = TextView(ctx)
        txt.gravity = Gravity.CENTER
        txt.setPadding(0, 16, 16, 16)
        txt.textSize = 16f
        txt.text = asr[i]
        txt.setTextColor(Color.parseColor("#000000"))
        return txt
    }

    fun getPosition(value:String):Int{
        return asr.indexOf(value)
    }
}