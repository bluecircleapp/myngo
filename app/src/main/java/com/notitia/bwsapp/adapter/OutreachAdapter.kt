package com.notitia.bwsapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.notitia.bwsapp.R
import com.notitia.bwsapp.data.db.entities.Outreach
import java.text.DateFormat

class OutreachAdapter(patients:ArrayList<Outreach>?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mRecyclerViewItems: ArrayList<Outreach>? = patients

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.outreach_title)
        var location: TextView = view.findViewById(R.id.location)
        var duration: TextView = view.findViewById(R.id.date_duration)

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.outreach_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        try {
            val menuItemHolder = holder as MyViewHolder

            val item: Outreach = mRecyclerViewItems!![position]
            val startDate =
                DateFormat.getDateInstance(DateFormat.MEDIUM)
                    .format(item.startDate!!)
            val endDate =
                DateFormat.getDateInstance().format(item.endDate!!)
            menuItemHolder.name.text = "${item.name}"
            menuItemHolder.location.text = "${item.location}"
            menuItemHolder.duration.text = "$startDate to $endDate"
        } catch (e: Exception) {
        }
    }
    override fun getItemCount(): Int {
        return mRecyclerViewItems!!.size
    }
}